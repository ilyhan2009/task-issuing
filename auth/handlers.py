from textwrap import dedent
import uuid

from aiohttp import web
from aiohttp_security import (
    remember, forget, authorized_userid,
    check_permission, check_authorized,
)
import aiohttp_jinja2

from .db_auth import check_credentials, store_token, get_token


class AuthHandlers(object):
    @aiohttp_jinja2.template('index_login.jinja2')
    async def index(self, request):
        login = await authorized_userid(request)
        if login:
            return {'authorized': True, 'login': login}
        return {'authorized': False}

    async def login(self, request):
        response = web.HTTPFound('/')
        form = await request.post()
        login = form.get('login')
        password = form.get('password')
        db_engine = request.app.db_engine

        authorized, user_id = await check_credentials(db_engine, login, password)
        if authorized:
            token = await get_token(db_engine, user_id)
            if not token:
                token = uuid.uuid4().hex
                await store_token(db_engine, user_id, token)
            await remember(request, response, token)
            raise response

        raise web.HTTPUnauthorized(
            body=b'Invalid username/password combination')

    def configure(self, app):
        router = app.router
        router.add_route('GET', '/', self.index, name='index')
        router.add_route('POST', '/login', self.login, name='login')
