CREATE USER task_issuing_user WITH PASSWORD 'password';
DROP DATABASE IF EXISTS task_issuing;
CREATE DATABASE task_issuing;
ALTER DATABASE task_issuing OWNER TO task_issuing_user;
GRANT ALL PRIVILEGES ON DATABASE task_issuing TO task_issuing_user;