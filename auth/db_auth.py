import sqlalchemy as sa

from aiohttp_security.abc import AbstractAuthorizationPolicy
from passlib.hash import sha256_crypt

from db import schema


class DBAuthorizationPolicy(AbstractAuthorizationPolicy):
    def __init__(self, dbengine):
        self.dbengine = dbengine

    async def authorized_userid(self, token):
        async with self.dbengine.acquire() as conn:
            where = schema.tokens.c.token == token
            query = schema.tokens.join(schema.users).select().where(where)
            ret = await conn.execute(query)
            user_token = await ret.fetchone()
            if user_token:
                return user_token.login
            else:
                return None

    async def permits(self, identity, permission, context=None):  # todo has to be implemented later
        if identity is None:
            return False
        return True
        #
        # async with self.dbengine.acquire() as conn:
        #     where = sa.and_(schema.users.c.login == identity,
        #                     sa.not_(schema.users.c.disabled))
        #     query = schema.users.select().where(where)
        #     ret = await conn.execute(query)
        #     user = await ret.fetchone()
        #     if user is not None:
        #         user_id = user[0]
        #         is_superuser = user[3]
        #         if is_superuser:
        #             return True
        #
        #         where = schema.permissions.c.user_id == user_id
        #         query = schema.permissions.select().where(where)
        #         ret = await conn.execute(query)
        #         result = await ret.fetchall()
        #         if ret is not None:
        #             for record in result:
        #                 if record.perm_name == permission:
        #                     return True
        #
        #     return False


async def check_credentials(db_engine, username, password):
    async with db_engine.acquire() as conn:
        where = sa.and_(schema.users.c.login == username,
                        sa.not_(schema.users.c.disabled))
        query = schema.users.select().where(where)

        ret = await conn.execute(query)
        user = await ret.fetchone()
        if user is not None:
            hashed = user[2]
            return sha256_crypt.verify(password, hashed), user.id
    return False, False


async def store_token(db_engine, user_id, token):
    async with db_engine.acquire() as conn:
        query = schema.tokens.insert().values({
            'token': token,
            'user_id': user_id,
        })
        await conn.execute(query)


async def get_token(db_engine, user_id):
    async with db_engine.acquire() as conn:
        where = schema.tokens.c.user_id == user_id
        query = schema.tokens.select().where(where)
        ret = await conn.execute(query)
        token_row = await ret.fetchone()
        if token_row is not None:
            return token_row.token
