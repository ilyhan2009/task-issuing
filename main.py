from aiohttp import web
from aiohttp_security import setup as setup_security
from aiohttp_security import CookiesIdentityPolicy
from aiopg.sa import create_engine

from auth.db_auth import DBAuthorizationPolicy
from auth.handlers import AuthHandlers

from static import StaticHandler
from templates import Templates


async def make_app():

    db_engine = await create_engine(dsn='postgresql://task_issuing_user:password@172.101.0.15:5432/task_issuing')
    app = web.Application()

    app.db_engine = db_engine
    app.on_shutdown.append(on_shutdown)
    setup_security(app,
                   CookiesIdentityPolicy(),
                   DBAuthorizationPolicy(db_engine))

    auth_handlers = AuthHandlers()
    auth_handlers.configure(app)

    static_handler = StaticHandler()
    static_handler.configure(app)

    templates = Templates()
    templates.configure(app)

    return app


async def on_shutdown(app):
    app.db_engine.close()
    await app.db_engine.wait_closed()


def main():
    app = make_app()
    web.run_app(app, host='127.0.0.1', port=8080)


if __name__ == '__main__':
    main()
