from aiohttp import web

import os


class StaticHandler(object):
    def configure(self, app):
        router = app.router
        router.add_routes([web.static('/static', os.path.join(os.path.dirname(__file__), 'static'))])
        app['static_root_url'] = 'static'
