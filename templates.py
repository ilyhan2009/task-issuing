import aiohttp_jinja2
import jinja2

import os


class Templates(object):
    def configure(self, app):
        aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')))
