import sqlalchemy as sa
from sqlalchemy.sql import expression

metadata = sa.MetaData()


users = sa.Table(
    'users', metadata,
    sa.Column('id', sa.Integer, nullable=False, primary_key=True, autoincrement=True),
    sa.Column('login', sa.String(256), nullable=False),
    sa.Column('password', sa.String(256), nullable=False),
    sa.Column('disabled', sa.Boolean, nullable=False, server_default=expression.false()),

    # indices
    sa.UniqueConstraint('login', name='user_login_key'),
)


roles = sa.Table(
    'roles', metadata,
    sa.Column('id', sa.Integer, nullable=False, primary_key=True, autoincrement=True),
    sa.Column('user_id', sa.Integer, nullable=False),
    sa.Column('role', sa.String(64), nullable=False),

    # indices
    sa.ForeignKeyConstraint(['user_id'], [users.c.id],
                            name='user_role_fkey',
                            ondelete='CASCADE'),
)

tokens = sa.Table(
    'tokens', metadata,
    sa.Column('id', sa.Integer, nullable=False, primary_key=True, autoincrement=True),
    sa.Column('user_id', sa.Integer, nullable=False),
    sa.Column('token', sa.String(256), nullable=False),
    # sa.Column('created', sa.DateTime, nullable=False),

    # indices
    sa.UniqueConstraint('user_id', name='user_token_key'),
    sa.ForeignKeyConstraint(['user_id'], [users.c.id],
                            name='user_token_fkey',
                            ondelete='CASCADE'),
)
